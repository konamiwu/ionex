//
// Created by Konami on 2023/6/19.
//

import Foundation
import UIKit

class AppCoordinator {
    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let loginVC = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        loginVC.viewModel = LoginViewModel(coordinator: LoginCoordinator(navigationController: navigationController))
        navigationController.viewControllers = [loginVC]
    }
}
