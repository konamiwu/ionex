//
//  ViewController.swift
//  Ionex
//
//  Created by Konami on 2023/6/17.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var resultLabel: UILabel!

    private let username = "test2@qq.com"
    private let password = "test1234qq"
    private let loginFailedMessage = "Login failed"

    var viewModel: LoginViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
    }

    @IBAction func loginAction() {
        resultLabel.text = ""

        guard let username = usernameTextField.text,
              let password = passwordTextField.text else {
            resultLabel.text = "input error"
            return
        }

        viewModel?.login(username: username, password: password)
    }
}

extension ViewController: LoginViewModelDelegate {
    func loginFailed() {
        let action = UIAlertAction(title: "Confirm", style: .default)
        let alertController = UIAlertController(title: nil, message: loginFailedMessage, preferredStyle: .alert)
        alertController.addAction(action)
        present(alertController, animated: true)
    }
}
