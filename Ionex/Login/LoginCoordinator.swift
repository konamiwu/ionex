//
// Created by Konami on 2023/6/19.
//

import Foundation
import UIKit

class LoginCoordinator: LoginCoordinatorProtocol {
    private var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func showSuccess(objectId: String) {
        let viewModel = UpdateViewModel(objectId: objectId)
        let vc = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "UpdateViewController") as! UpdateViewController
        vc.viewModel = viewModel
        navigationController.pushViewController(vc, animated: true)
    }
}
