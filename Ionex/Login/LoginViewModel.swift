//
// Created by Konami on 2023/6/19.
//

import Foundation

protocol LoginViewModelDelegate: AnyObject {
    func loginFailed()
}

class LoginViewModel {
    private var coordinator: LoginCoordinatorProtocol

    weak var delegate: LoginViewModelDelegate?
    init(coordinator: LoginCoordinatorProtocol) {
        self.coordinator = coordinator
    }

    func login(username: String, password: String) {
        let request = LoginRequest(username: username, password: password)
        request.callback = { response in
            print("response.sessionToken = \(response.sessionToken)")
            APIs.instance.token = response.sessionToken
            DispatchQueue.main.async {
                self.coordinator.showSuccess(objectId: response.objectId)
            }
        }

        request.errorHandler = { _, _ in
            self.delegate?.loginFailed()
        }
        request.execute()
    }
}
