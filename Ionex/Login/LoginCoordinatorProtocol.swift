//
// Created by Konami on 2023/6/19.
//

import Foundation

protocol LoginCoordinatorProtocol: AnyObject {
    func showSuccess(objectId: String)
}
