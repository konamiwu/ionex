//
// Created by Konami on 2023/6/19.
//

import Foundation

protocol UpdateViewModelDelegate: AnyObject {
    func updateSuccess()
    func updateFailed()
}

class UpdateViewModel {
    private let objectId: String

    weak var delegate: UpdateViewModelDelegate?

    deinit {
        print("UpdateViewModel deinit")
    }
    init(objectId: String) {
        self.objectId = objectId
    }

    func update(timezone: Int) {
        let request = UpdateTimezoneRequest(objectId: objectId, timezone: timezone)
        request.callback = { _ in
            self.delegate?.updateSuccess()
        }

        request.errorHandler = { _, _ in
            self.delegate?.updateFailed()
        }

        request.execute()
    }
}
