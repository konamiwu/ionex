//
// Created by Konami on 2023/6/19.
//

import Foundation
import UIKit

class UpdateViewController: UIViewController {
    @IBOutlet var textField: UITextField!
    @IBOutlet var resultLabel: UILabel!

    private let typeErrorMessage: String = "Input type mismatched"
    private let updateSuccessMessage: String = "Update successfully"
    private let updateFailedMessage: String = "update failed"

    var viewModel: UpdateViewModel?

    deinit {
        print("UpdateViewController deinit")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
    }

    @IBAction func updateButtonAction() {
        resultLabel.text = ""
        guard let text = textField.text,
           let value = Int(text) else {
            resultLabel.text = typeErrorMessage
            return
        }
        viewModel?.update(timezone: value)
    }
}

extension UpdateViewController: UpdateViewModelDelegate {
    func updateSuccess() {
        DispatchQueue.main.async {
            self.resultLabel.text = self.updateSuccessMessage
        }
    }

    func updateFailed() {
        DispatchQueue.main.async {
            self.resultLabel.text = self.updateFailedMessage
        }
    }
}
