//
// Created by Konami on 2023/6/19.
//

import Foundation

class UpdateTimezoneRequest: BaseRequest<UpdateTimezoneRequest.UpdateTimeZoneResponse> {
    class UpdateTimeZoneResponse: Decodable {
        let updatedAt: String
    }
    init(objectId: String, timezone: Int) {
        let urlString = "\(APIs.update)/\(objectId)"
        super.init(url: URL(string: urlString)!)

        let body: [String: Any?] = [
            "timezone": timezone
        ]
        guard let requestBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        else { return }
        urlRequest.httpBody = requestBody
        urlRequest.httpMethod = "PUT"
    }

    override func debug(data: Data?, response: URLResponse?, error: Error?) {
        if let data = data, let string = String(data: data, encoding: .utf8) {
            print("UpdateTimezoneResponse string = \(string)")
        }
    }

    override func decode(data: Data) -> UpdateTimeZoneResponse? {
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(UpdateTimeZoneResponse.self, from: data)
        } catch let error {
            print("error decode: \(error)")
            return nil
        }
    }
}
