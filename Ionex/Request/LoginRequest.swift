//
// Created by Konami on 2023/6/18.
//

import Foundation

class LoginRequest: BaseRequest<LoginRequest.LoginResponse> {
    class LoginResponse: Decodable {
        let objectId: String
        let sessionToken: String
    }

    init(username: String, password: String) {
        super.init(url: URL(string: APIs.login)!)

        let body: [String: Any?] = [
            "username": username,
            "password": password
        ]
        guard let requestBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
        else { return }
        urlRequest.httpBody = requestBody
        urlRequest.httpMethod = "POST"
    }

    override func debug(data: Data?, response: URLResponse?, error: Error?) {
        if let data = data, let string = String(data: data, encoding: .utf8) {
            print("string = \(string)")
        }
    }

    override func decode(data: Data) -> LoginResponse? {
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(LoginResponse.self, from: data)
        } catch let error {
            print("error decode: \(error)")
            return nil
        }
    }
}
