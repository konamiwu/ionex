//
// Created by Konami on 2023/5/24.
//

import Foundation

final class APIs {
    private init() { }

    static let login: String = "https://watch-master-staging.herokuapp.com/api/login"
    static let update: String = "https://watch-master-staging.herokuapp.com/api/users"
    static let instance = APIs()

    private let X_Parse_Application_Id: String = "vqYuKPOkLQLYHhk4QTGsGKFwATT4mBIGREI2m8eD"
    var token: String = ""

    func jsonRequest(url: URL) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(X_Parse_Application_Id, forHTTPHeaderField: "X-Parse-Application-Id")
        print("token = \(token)")
        urlRequest.setValue(token, forHTTPHeaderField: "X-Parse-Session-Token")
        urlRequest.timeoutInterval = 10
        return urlRequest
    }
}